# ispb-website

Final project of Web programming Tecnologies Course: A mockup of website of the Instituto Politecnico do Bie


## Usage
Open a terminal in the root project and run:

```
$ python -m http.server 8000
```

then open http://127.0.0.0.1:8000

## License
GPL V3


## Contributing

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)